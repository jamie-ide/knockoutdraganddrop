﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KnockoutDragAndDrop.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Html5Sortable()
        {
            return View();
        }

        public ActionResult KoDragDrop()
        {
            return View();
        }

        public ActionResult RollMyOwn()
        {
            return View();
        }
    }
}